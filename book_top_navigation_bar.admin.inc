<?php
/**
 * @file
 * Administration page callbacks for the book_top_navigation_bar module.
 */

/**
 * Form builder. Configure when the navigation appears.
 */
function book_top_navigation_bar_admin_settings() {
  $form['#validate'][] = 'book_top_navigation_bar_validate_admin_settings';

  $form['book_top_navigation_bar_show_child_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show child links'),
    '#default_value' =>
      variable_get(
        'book_top_navigation_bar_show_child_links',
        BOOKTOPNAV_DEFAULT_SHOW_CHILD_LINKS
      ),
    '#description' => t(
      'Show links to child pages in the top navigation bar.'
    ),
  );

  $form['book_top_navigation_bar_min_content_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum content length'),
    '#default_value' =>
      variable_get(
        'book_top_navigation_bar_min_content_length',
        BOOKTOPNAV_DEFAULT_MIN_CONTENT_LENGTH
      ),
    '#description' => t(
        'Content must be at least this many characters long before '
      . 'the top navigation bar will appear.'
    ),
  );
  return system_settings_form($form);
}

/**
 * Validate data in the admin form.
 */
function book_top_navigation_bar_validate_admin_settings($form, &$form_state) {
  $min_length = trim($form_state['values']['book_top_navigation_bar_min_content_length']);
  $min_length = check_plain($min_length);
  if ( is_nan($min_length) || $min_length < 0 ) {
    form_set_error(
      'book_top_navigation_bar_min_content_length',
      t('Sorry, the minimum length should be zero or greater.')
    );
    return;
  }
}
