
-- SUMMARY --

The book top navigation bar module takes the navigation bar that appears
at the bottom of each book page, and repeats it at the top of the page.
This can be especially useful for users who are scanning a book, looking
for something they have seen before.

You can specify whether the current page's child links are shown in the top navigation
bar. You can also specify how long a page must be before the navigation
bar is added. Repeated navigation can look strange on short pages.


-- REQUIREMENTS --

The book module must be enabled.


-- INSTALLATION --

Install as usual. eee http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

Go to admin/config/user-interface/booktopnav to configure. Set the minimum number of
characters of content that must be present before the top navigation appears.
You can also choose whether the child links are shown or not.


-- CUSTOMIZATION --

For convenient CSS styling, the class book-navigation-top is assigned to 
the navigation bar in the default template, book-navigation-top.tpl.php. 
For more extensive themeing, copy book-navigation-top.tpl.php to your 
own theme, and edit the file.


-- CONTACT --

Current maintainer:  Kieran Mathieson, kieran@dolfinity.com

This project is sponsored by Dolfinity Learning.
